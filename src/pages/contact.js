import React from "react"
import { Form, InputGroup, Button } from "react-bootstrap"
import Layout from "../components/layout"
import SEO from "../components/seo"

export default class IndexPage extends React.Component {
  render() {
    return (
      <Layout>
        <SEO title={"Contact Form"}/>
        <Form method="post" netlify-honeypot="bot-field" data-netlify="true">
          <input type="hidden" name="bot-field"/>
          <label for={"name"}>Name</label>
          <input class="form-control" type="text" name="name" id="name"/>
          <label for={"email"}>Email</label>
          <input class="form-control" type="text" name="email" id="email"/>
          <label for={"subject"}>Subject</label>
          <input class="form-control" type="text" name="subject" id="subject"/>
          <label for={"content"}>Content</label>
          <textarea class="form-control" type="text" name="name" id="content"/>
          <Button type="submit">Send</Button>
          <div data-netlify-recaptcha="true"></div>
        </Form>
      </Layout>
    )
  };
}