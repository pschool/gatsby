import React from "react"
import PropTypes from "prop-types"
import { graphql } from "gatsby"
import SEO from "../../components/seo"
import Layout from "../../components/layout"
import { Table } from "react-bootstrap"

const ProductsListPages = ({
                             data: {
                               allMarkdownRemark: { edges }
                             }
                           }) => (
  <Layout>
    <div>
      <SEO title="Test"/>
      <div>
        <h1>Product</h1>
        <Table>
          <thead>
          <tr>
            <th>Titre</th>
            <th>price</th>
            <th>desciption</th>
            <th>image</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          {edges.map(edges => (
            <tr>
              <th>{edges.node.frontmatter.title}</th>
              <th>{edges.node.frontmatter.price} €</th>
              <th>{edges.node.frontmatter.description.substring(0, 25)} ...</th>
              <th><img src={edges.node.frontmatter.image}/></th>
              <th><a class="btn btn-outline-success" href={"/products/".concat(edges.node.frontmatter.title)}>voir la
                fiche</a></th>
            </tr>
          ))}
          </tbody>
        </Table>
      </div>
    </div>
  </Layout>
)


ProductsListPages.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            frontmatter: PropTypes.shape({
              description: PropTypes.string.isRequired,
              image: PropTypes.string.isRequired,
              price: PropTypes.string.isRequired,
              title: PropTypes.string.isRequired
            })
          })
        }).isRequired
      )
    })
  })
}

export default ProductsListPages

export const pageQuery = graphql`
    query {
        allMarkdownRemark(filter: {frontmatter: {type:{eq: "products"}}}) {
            totalCount
            edges {
                node {
                    frontmatter{
                        description
                        image
                        price
                        title
                        type
                    }
                }
            }
        }
    }
`