import React from "react"
import { Navbar, Nav } from "react-bootstrap"

export default () => (
  <>
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="/">Full Stack M1</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav"/>
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/">Home</Nav.Link>
          <Nav.Link href="/products">Products</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </>
)