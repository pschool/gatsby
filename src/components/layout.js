import React from "react"
import NavBar from "./nav-bar"
import "bootstrap/dist/css/bootstrap.min.css"

const Layout = ({ children }) => (
  <>
    <NavBar/>
    <div className="container">
      {children}
    </div>
  </>
)
export default Layout