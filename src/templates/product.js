import React from "react"
import { graphql } from "gatsby"
import SEO from "../components/seo"
import Layout from "../components/layout"
import PropTypes from "prop-types"
import Markdown from "markdown-to-jsx"
import { Container, Row, Col, Image } from "react-bootstrap"

const ProductPages = ({
                        data: {
                          allMarkdownRemark: { edges }
                        }
                      }) => (
  <Layout>
    <Container>
      <SEO title={edges[0].node.frontmatter.title}/>
      <h1>{edges[0].node.frontmatter.title} <small>{edges[0].node.frontmatter.price} €</small></h1>
      <Row>
        <Col>
          <Markdown>{edges[0].node.frontmatter.description}</Markdown>
        </Col>
        <Col>
          <Image src={edges[0].node.frontmatter.image}/>
        </Col>
      </Row>
    </Container>
  </Layout>
)
export default ProductPages
ProductPages.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            frontmatter: PropTypes.shape({
              description: PropTypes.string.isRequired,
              image: PropTypes.string.isRequired,
              price: PropTypes.string.isRequired,
              title: PropTypes.string.isRequired
            })
          })
        }).isRequired
      )
    })
  })
}
export const pageQuery = graphql`
    query($slug: String) {
        allMarkdownRemark(filter: {frontmatter: {title:{eq: $slug}}}) {
            totalCount
            edges {
                node {
                    frontmatter{
                        description
                        image
                        price
                        title
                        type
                    }
                }
            }
        }
    }
`
