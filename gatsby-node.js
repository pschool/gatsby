const path = require(`path`)

const makeRequest = (graphql, request) => new Promise((resolve, reject) => {
  // Query for nodes to use in creating pages.
  resolve(
    graphql(request).then(result => {
      if (result.errors) {
        reject(result.errors)
      }

      return result
    })
  )
})

// Implement the Gatsby API “createPages”. This is called once the
// data layer is bootstrapped to let plugins create pages from data.
exports.createPages = ({ boundActionCreators, graphql }) => {
  const { createPage } = boundActionCreators

  const getProduct = makeRequest(graphql, `
      {
          allMarkdownRemark {
              totalCount
              edges {
                  node {
                      frontmatter{
                          description
                          image
                          price
                          title
                          type
                      }
                  }
              }
          }
      }
  `).then(result => {
    // Create pages for each article.
    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
      createPage({
        path: `/products/${node.frontmatter.title}`,
        component: path.resolve(`${__dirname}/src/templates/product.js`),
        context: {
          title: node.frontmatter.title
        }
      })
    })
  })

  // Query for articles nodes to use in creating pages.
  return getProduct
}